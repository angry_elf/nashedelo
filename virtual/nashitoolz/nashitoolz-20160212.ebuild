# Distributed under the terms of the GNU General Public License v2
EAPI="3"
DESCRIPTION="Virtual for all system tools we need"
HOMEPAGE=""
SRC_URI=""
LICENSE=""
SLOT="1"
KEYWORDS="amd64"
IUSE=""

RDEPEND="
	sys-kernel/gentoo-sources
	app-portage/layman[git,mercurial]
	app-portage/portage-utils
	app-portage/eix
	app-portage/genlop
	app-admin/pydf
	sys-fs/ncdu
	app-portage/gentoolkit
	app-admin/cronolog
	app-admin/logrotate
	app-admin/monit
	app-admin/pwgen
	app-admin/syslog-ng
	app-admin/sysstat
	app-admin/sudo
	app-editors/nano
	app-misc/grc
	app-misc/mc
	app-misc/tmux
	dev-python/pip
	mail-mta/exim
	net-analyzer/munin[minimal]
	net-analyzer/netcat
	net-analyzer/tcpdump
	net-analyzer/iftop
	net-dns/bind-tools
	net-firewall/iptables
	net-misc/ntp
	net-misc/whois
	sys-apps/sdparm
	sys-fs/lvm2
	sys-fs/mdadm
	sys-fs/reiserfsprogs
	sys-process/htop
	sys-process/iotop
	sys-process/lsof
	sys-process/vixie-cron
	sys-apps/iproute2
    sys-apps/pciutils
    sys-apps/smartmontools[minimal]
    sys-apps/lm_sensors
    
        "
DEPEND=""


# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v3
# $Header: /var/cvsroot/gentoo-x86/media-sound/volti/volnoti-0.1.ebuild $

EAPI=8

DESCRIPTION="Systray applet for PulseAudio (and PipeWire)"
HOMEPAGE="https://github.com/fernandotcl/pa-applet"
SRC_URI="https://github.com/fernandotcl/pa-applet/archive/refs/heads/master.zip -> ${P}.zip"


LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""


DEPEND=""
RDEPEND=""


S="${WORKDIR}/${PN}-master"

src_prepare() {
    eapply_user
    eautoreconf
}

src_configure() {
   ./autogen.sh
    econf
}

src_compile() {
    emake
}

src_install() {
    emake DESTDIR="${D}" install
}

pkg_postinst() {
    elog "pa-applet has been installed"
}

pkg_postrm() {
    elog "pa-applet has been removed"
}
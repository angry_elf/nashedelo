# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
DESCRIPTION="perl script that is a munin plugin for couchdb"
HOMEPAGE="https://github.com/strattg/munin-plugin-couchdb"
EGIT_REPO_URI="git://github.com/strattg/munin-plugin-couchdb"
inherit  git-2
LICENSE="GPL-2"
SLOT=0

src_install() { 
		dodir /usr/libexec/
		dodir /usr/libexec/munin/
		dodir /usr/libexec/munin/plugins
		cp -R "${S}/" "${D}/" || die "install failed!"
	      }

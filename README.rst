**Overlay installation manual**

We are now in  layman repository list.

1. Check that layman is compiled with *mercurial* and *git* support. 
2. If it doesn't - run: "USE="mercurial git" emerge -av layman".
3. Run: "layman -o https://bitbucket.org/angry_elf/nashedelo/raw/master/nashedelo.xml -a nashedelo" ;
4. Then run: "layman -S" ;
5. And finally: "eix-update" .
6. Thats all!

# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7

DESCRIPTION="open source database for building cloud services"
HOMEPAGE="https://www.cockroachlabs.com"
SRC_URI="https://binaries.cockroachdb.com/cockroach-v${PV}.linux-amd64.tgz"


LICENSE="Cockroach Apache-2.0"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

S="${WORKDIR}/cockroach-v${PV}.linux-amd64"

RDEPEND="
    acct-user/cockroach
    acct-group/cockroach"

src_install() {
        dobin cockroach
        insinto /etc/security/limits.d
        newins "${FILESDIR}"/cockroach-limits.conf cockroach.conf
        newconfd "${FILESDIR}"/cockroach.confd-1.0 cockroach
        newinitd "${FILESDIR}"/cockroach.initd-1.1 cockroach
        keepdir /var/log/cockroach
        fowners cockroach:cockroach /var/log/cockroach
        if [[ -z ${REPLACING_VERSIONS} ]]; then
                ewarn "The default setup is for the first node of an insecure"
                ewarn "cluster that only listens on localhost."
                ewarn "Please read the cockroach manual at the following url"
                ewarn "and configure /etc/conf.d/cockroach correctly if you"
                ewarn "plan to use it in production."
                ewarn
                ewarn "http://cockroachlabs.com/docs"
        fi
}

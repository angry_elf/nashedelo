#!/sbin/runscript
# Init-script for loading flashcache device.

DEVICE=${CACHED_DEV};

start() {
	# Load module if not already loaded
	if [[ ! "$(eval lsmod | grep "^flashcache")" ]]; then
 		ebegin "Loading ${SVCNAME} kernel module"
		modprobe flashcache || eerror "Failed to load kernel module"
	fi
	# Checking if cache device needs to be loaded
	NEED_LOAD=0;
	if [ ! -e /dev/mapper/${DEVICE} ]; then
		NEED_LOAD=1;
	fi

	# Loading cache-device if needed
	if [ ${NEED_LOAD} ]; then
		ebegin "Loading ${SVCNAME} from ${DEVICE}"
        	flashcache_load ${CACHE_DEV} || eerror "Failed to load FlashCache from  ${CACHE_DEV}"
	fi	
	
}


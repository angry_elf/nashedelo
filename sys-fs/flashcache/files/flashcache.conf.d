# /etc/conf.d/flashcache: config file for /etc/init.d/flashcache

# Specify cache-divice and cached device names to be loaded.
# E.g for if you've created your device by running:
# 'flachache_create -p back flashcache1 /dev/sdc /dev/sda2"  
 
# CACHE_DEV="/dev/sdc"
# CACHED_DEV="flashcache1"

# For more than one cache-devices you will need to load them separately. 
# You can create symbolic links to flashcache service like so
#   cd /etc/init.d
#   ln -s flashcache flashcache.foo
#   cd ../conf.d
#   cp flashcache flashcache.foo
# Now you can edit flashcache.foo and specify a different cache-device.



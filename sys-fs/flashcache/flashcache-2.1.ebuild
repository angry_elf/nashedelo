# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2


EAPI=4
if [ ${PV} != 9999 ]; then
	EGIT_BRANCH="stable_v${PV}"
fi

inherit eutils git-2 linux-info linux-mod

DESCRIPTION="Write back block cache Linux kernel module and tools"
HOMEPAGE="https://github.com/facebook/flashcache"
EGIT_REPO_URI="https://github.com/facebook/flashcache.git"
ETYPE="sources"

LICENSE="GPL-2"
SLOT="0"

[[ ${PV} == *9999* ]] || KEYWORDS="~amd64 ~arm ~ppc ~x86 ~amd64-linux ~x86-linux"

IUSE="doc"

RDEPEND="dev-vcs/git"
DEPEND="${RDEPEND}"

#CONFIG_CHECK="MD BLK_DEV_DM DM_UEVENT"

S=${WORKDIR}

#ARCH="x86"
BUILD_TARGETS=" "
MODULE_NAMES="flashcache(extra:${S}/src)"

src_compile() {
         set_arch_to_kernel
 
         emake KERNEL_TREE="${KERNEL_DIR}" || die "Compile fialed" 
 
 }
 
src_install() {
         linux-mod_src_install || die "install failed"
 
         (
                 cd "${S}/src/utils"
                 dosbin flashcache_create flashcache_destroy flashcache_load || die "Install failed"
         )
 
        dodoc README* doc/* || die "Install failed"

        newconfd "${FILESDIR}"/"${PN}".conf.d flashcache
        newinitd "${FILESDIR}"/"${PN}".init.d flashcache
	
	ewarn "IF you;ve created cached device that contains LVM you will have to add in your rc.conf something like"
        ewarn " rc_lvm_need=\"flashcache\" or rc_lvm_after=\"flashcache\"  for LVM to be waiting untill flashcache is"
	ewarn "loaded "
        ewarn " "
        ewarn "NOTE, that if you also have cached devices for , e.g lvm logical volume, you will have to load them"
        ewarn "through the separate init-scrip by creating symbolic link to /etc/init.d/flashcache, and make it start"
        ewarn "after LVM. You will also need separate config in /etc/conf.d/"

 }

# Copyright 2019-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit acct-user

ACCT_USER_ID=-1
ACCT_USER_GROUPS=( cockroach )
ACCT_USER_HOME=/var/lib/cockroach

acct-user_add_deps

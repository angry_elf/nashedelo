# Copyright 2019-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

DESCRIPTION="User for CouchDB database"
ACCT_USER_ID=-1
ACCT_USER_GROUPS=( couchdb )
ACCT_USER_HOME=/var/lib/couchdb

acct-user_add_deps

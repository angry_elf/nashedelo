 
#SRC_URI="http://github.com/bitly/nsq/archive/v${PV}.zip"
EGIT_REPO_URI="https://github.com/bitly/nsq.git"
IUSE="pynsq supervisor"
SLOT='0' 
KEYWORDS="~amd64"
DEPEND="dev-lang/go
	pynsq? ( dev-python/pynsq )
	supervisor? ( app-admin/supervisor )
	"
# include functions from eutils 
inherit eutils

pkg_setup() {
	if [ ! -f /usr/lib/go/src/pkg/github.com/bitly/go-notify/notify.go ]; then
		die "You need go-notify. Install it with 'go get github.com/bitly/go-notify' "
	elif [ ! -f /usr/lib/go/src/pkg/github.com/bitly/go-simplejson/simplejson.go ]; then
		die "You need go-simplejson. Install it with 'go get github.com/bitly/go-simplejson' "
	elif [ ! -f /usr/lib/go/src/pkg/github.com/bmizerany/assert/assert.go ]; then
		die "You need assert. Install it with 'go get github.com/bmizerany/assert' "
	fi
	#Create nsq user and group
        enewgroup nsq
        enewuser nsq -1 -1 /var/lib/nsq nsq

}

src_unpack() {
    git clone https://github.com/bitly/nsq.git ${A} || die "Failed to get source"
    cd nsq
    if [ ${PV} != 9999 ]; then
	    git checkout v${PV}
    fi
    sed -i 's/PREFIX=\/usr\/local/PREFIX=\/usr/g' Makefile
    epatch "${FILESDIR}"/*.patch
}

src_compile() {
	cd "${PN}"
	emake || die "Compile failed"
}

src_install() {
	cd "${PN}"
	emake DESTDIR="${D}" install || die "Install failed"
	diropts -m 0770 -o nsq -g nsq
        keepdir /var/log/nsq
	
	if use supervisor; then
		diropts -o root -g root
		keepdir /etc/supervisor.d
		insinto /etc/supervisor.d/   
                doins "${FILESDIR}"/nsq.conf 
	fi
	return	
}


pkg_postinst() {
	go get github.com/bitly/go-nsq || die "Failed to get nsq package for GO"
}

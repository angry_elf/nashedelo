# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=8

DESCRIPTION="A realtime distributed messaging platform"
HOMEPAGE="http://nsq.io/"
SRC_URI="https://s3.amazonaws.com/bitly-downloads/nsq/nsq-${PV}.linux-amd64.go1.6.2.tar.gz"

#inherit user

IUSE="supervisor"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64"

S="${WORKDIR}/nsq-${PV}.linux-amd64.go1.6.2"

RDEPEND="
    !net-misc/nsq
    acct-user/nsq
    acct-group/nsq
"

DEPEND="${RDEPEND}
	supervisor? ( app-admin/supervisor )
	"

src_install() {
    dodir "/opt/nsq"
    cp -R "${S}/"* "${D}/opt/nsq" || die "Install failed!"

	if use supervisor; then
		diropts -o root -g root
		keepdir /etc/supervisord.d
		insinto /etc/supervisord.d/
                doins "${FILESDIR}"/nsq.conf
	fi
}

pkg_preinst() {
    diropts -m 0700 -o nsq -g nsq
    dodir /var/lib/nsq /var/log/nsq
}

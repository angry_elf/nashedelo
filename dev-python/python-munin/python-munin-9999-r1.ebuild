# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6
DESCRIPTION="python-munin"
HOMEPAGE="http://samuelks.com/python-munin/"
##SRC_URI="git://github.com/samuel/python-munin"
EGIT_REPO_URI="git://github.com/samuel/python-munin"
LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ia64 ppc ~ppc64 x86 ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos"
IUSE=""
DEPEND="dev-python/setuptools"


PYTHON_COMPAT=( python{2_6,2_7} )

inherit distutils-r1 git-r3


#src_install() {
#	distutils_src_install
#	
#}
